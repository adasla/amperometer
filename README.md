# Amperometer

Amperometer is a standalone desktop app for personal finances monitoring. Amperometer accepts bnk statements in format 
used by [Inteligo](https://www.inteligo.pl). Amperometer is written in TypeScript and employs Electron and Angular. 
Amperometer is built on top of [dream starter](https://github.com/colinskow/angular-electron-dream-starter).

## Features

* Transaction table
* Balance chart
* Spendings by category chart
* List of receivers
* Tags and rules for automatic tagging
* As of today only Polish language version (Inteligo is a Polish bank)

## Installation

For development environment:

```
npm install
npm start
```

For testing
```
npm test
```

## License
MIT
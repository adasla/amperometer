import { Component, ViewChild, OnInit } from '@angular/core';
import { MatSort, Sort } from '@angular/material';
import { Router } from '@angular/router';
import { TransactionsService } from '../../providers/transactions.service';
import { Transaction, SimpleDataSource, ObservableDataSource } from '../../providers/data-model.service';
import * as Collections from 'typescript-collections';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/combineLatest';
import 'rxjs/add/operator/merge';
import 'rxjs/add/observable/of';
import { DataSource } from '@angular/cdk/collections';


@Component({
  selector: 'app-receivers-table',
  templateUrl: './receivers-table.component.html',
})
export class ReceiversTableComponent implements OnInit {
  readonly displayedColumns = ['name', 'number', 'amount'];
  @ViewChild(MatSort) sort: MatSort;
  dataSource: DataSource<AggreagteTransaction>;

  constructor(private trsService: TransactionsService, private router: Router) { }

  ngOnInit(): void {
    const defaultSort: Sort = { active: 'number', direction: 'desc' };
    const sortObservable: Observable<Sort> = Observable.of(defaultSort).merge(this.sort.sortChange.asObservable());
    const data: AggreagteTransaction[] = agg(this.trsService.transactions);
    const dataObservable: Observable<AggreagteTransaction[]> = sortObservable.map(sort => sortAggregates(data, sort));
    this.dataSource = new ObservableDataSource(dataObservable);
  }

  goToFiltering(tragg) {
    this.router.navigateByUrl('/list/' + encodeURIComponent(tragg.name));
  }
}

function sortAggregates(aggs: AggreagteTransaction[], sort: Sort): AggreagteTransaction[] {
  const sortBy = sort.active === 'amount'
    ? [compareAggsByValue, compareAggsByNumber, compareAggsByName]
    : [compareAggsByNumber, compareAggsByValue, compareAggsByName];
  const sortF = sort.direction === 'desc'
    ? (a, b) => compareAggregates(a, b, sortBy)
    : (a, b) => compareAggregates(b, a, sortBy)
  return aggs.sort(sortF);
}

function agg(trs: Transaction[]): AggreagteTransaction[] {
  const mm = new Collections.MultiDictionary<string, Transaction>();
  trs.forEach(tr => {
    mm.setValue(tr.name, tr);
  }
  );
  const f: (string) => AggreagteTransaction = key => {
    return {
      name: key,
      number: mm.getValue(key).length,
      value: mm.getValue(key).reduce((acc, tr) => acc + tr.amount, 0)
    };
  }
  return mm.keys().map(f).sort(compareAggregates);
}

export interface AggreagteTransaction {
  name: string,
  number: number,
  value: number
}

function compareAggregates(a, b, fs = [compareAggsByNumber, compareAggsByValue, compareAggsByName]): number {
  const f: (x, y) => number = fs.reduceRight<(x1, y1) => number>((acc, f1) => (a1, b1) => f1(a1, b1, acc), (a2, b2) => 0);
  return f(a, b);
}

function compareAggsByNumber(a, b, f): number {
  if (a.number < b.number) {
    return 1;
  }
  if (a.number > b.number) {
    return -1;
  }
  return f(a, b);
}

function compareAggsByValue(a, b, f): number {
  if (Math.abs(a.value) < Math.abs(b.value)) {
    return 1;
  }
  if (Math.abs(a.value) > Math.abs(b.value)) {
    return -1;
  }
  return f(a, b);
}

function compareAggsByName(a, b, f): number {
  if (a.name < b.name) {
    return 1;
  }
  if (a.name > b.name) {
    return -1;
  }
  return f(a, b);
}

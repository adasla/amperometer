import { Component, Input, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { MatChipInputEvent, MatAutocompleteSelectedEvent } from '@angular/material';
import { TagsService } from '../../providers/tags.service';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/startWith';
import { Tag } from '../../providers/data-model.service';
import { Observable } from 'rxjs/Observable';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';

const COMMA = 188, ENTER = 13;

@Component({
  selector: 'app-chips',
  templateUrl: './chips.component.html',
})
export class ChipsComponent implements OnInit {
  @Input()
  editMode = false;
  readonly separatorKeysCodes = [COMMA];

  @Input()
  tags: number[] = [];

  chipInput = '';

  myControl: FormControl = new FormControl();

  filteredOptions: Observable<Tag[]>;

  constructor(private tagService: TagsService) { }

  get enrichedTags(): Tag[] {
    return this.tags.map(tid => this.tagService.tags.find(t => t.id === tid));
  }

  ngOnInit() {
    this.filteredOptions = this.myControl.valueChanges
       .startWith(null)
       .map(val =>
        this.tagService.tags.filter(tag => tag.label.includes(val) && this.tags.find(e => e === tag.id) === undefined));
  }


  async add(event: MatChipInputEvent) {
    console.log('add', event);
    const input = event.input;
    if (event.value) {
      const value = (event.value || '').trim();
      if (input) {
        input.value = '';
      }

      const tag: Tag = this.tagService.tags.find(t => t.label === value);
      const id: number = (tag === undefined)
        ? this.tagService.create(value)
        : tag.id;
      console.log('add, after await', value, tag, id);
      this.tags.push(id);
    }
  }

  remove(elem: number): void {
    const index = this.tags.indexOf(elem);

    if (index >= 0) {
      this.tags.splice(index, 1);
    }
  }

  optionSelected(event: MatAutocompleteSelectedEvent) {
    console.log('optionSelected', event);
    this.tags.push(event.option.value.id);
    this.chipInput = '';
  }

}

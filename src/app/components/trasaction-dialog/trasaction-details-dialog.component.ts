import { Component, OnInit, Inject } from '@angular/core';
import {MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';

@Component({
  selector: 'app-trasaction-dialog',
  templateUrl: './trasaction-details-dialog.component.html',
  styles: ['.full-width { width: 100%; }']
})
export class TrasactionDetailsDialogComponent {

  constructor(
    public dialogRef: MatDialogRef<TrasactionDetailsDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any) { }

    cancel() {
      this.dialogRef.close();
    }

}

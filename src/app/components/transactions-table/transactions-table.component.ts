import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, ParamMap, Router } from '@angular/router';
import { MatDialog, MatDialogRef } from '@angular/material';
import { TransactionsService } from '../../providers/transactions.service';
import { Transaction, SimpleDataSource, DataModelService,
  ObservableDataSource, compileAndFilter } from '../../providers/data-model.service';
import { TrasactionDetailsDialogComponent } from '../trasaction-dialog/trasaction-details-dialog.component';
import { Observable } from 'rxjs/Observable';

@Component({
  selector: 'app-transactions-table',
  templateUrl: './transactions-table.component.html',
  styles: ['.mat-form-field.longField{ width: 1000px }']
})
export class TransactionsTableComponent implements OnInit {
  readonly displayedColumns = ['date', 'type', 'amount', 'name', 'description', 'chips', 'comment'];
  dataSource = new ObservableDataSource(this.dmService.transactionsObservable);
  query: string;

  constructor(private trsService: TransactionsService, private router: Router,
    public dialog: MatDialog, private route: ActivatedRoute, private dmService: DataModelService) {
      console.log('tr-table constructor');
    }

  ngOnInit(): void {
    this.route.paramMap.forEach((params: ParamMap) => {
      if (params.has('query')) {
        this.query = 'tr.name === \'' + params.get('query') + '\'';
        this.filter(this.query);
      }
    });
  }

  openDialog(tr: Transaction ): void {
    const trCopy = Object.assign({}, tr, { tags: tr.tags.slice() });
    const dialogRef = this.dialog.open(TrasactionDetailsDialogComponent, {
      width: '550px',
      data: trCopy
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
      if (result) {
        Object.assign(tr, result);
        tr.tags.slice(0, tr.tags.length, ...result.tags);
      }
    });
  }

  filter(query: string) {
    this.dataSource =  new ObservableDataSource(this.dmService.transactionsObservable.map(compileAndFilter<Transaction>(query)));
  }

  addRule(query: string) {
    this.router.navigateByUrl('/rules/' + encodeURIComponent(query));
  }

}

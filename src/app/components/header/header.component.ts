import { Component, OnInit } from '@angular/core';
import { TransactionsService } from '../../providers/transactions.service';
import { ElectronService } from '../../providers/electron.service';
import { DataModelService } from '../../providers/data-model.service';
import {remote} from 'electron';

@Component({
  selector: 'app-header',
  template: `
  <mat-toolbar color="primary">
    <button mat-button routerLink="/list"  routerLinkActive="active">Lista transakcji</button>
    <button mat-button routerLink="/balance-chart"  routerLinkActive="active">Wykres stanu konta</button>
    <button mat-button routerLink="/spendings-chart"  routerLinkActive="active">Wykres wydatków</button>
    <button mat-button routerLink="/receivers"  routerLinkActive="active">Odbiorcy</button>
    <button mat-button routerLink="/rules"  routerLinkActive="active">Reguły</button>
    <button mat-button routerLink="/tags-table"  routerLinkActive="active">Tagi</button>
    <span class="example-fill-remaining-space"></span>
    <app-data-toolbar></app-data-toolbar>
  </mat-toolbar>`,
  styles: ['.example-fill-remaining-space { flex: 1 1 auto;}' ]
})
export class HeaderComponent  { }

@Component({
  selector: 'app-data-toolbar',
  template: `
    <button mat-raised-button (click)="import()"  color="accent">Importuj</button>
    <button mat-raised-button (click)="load()"  color="accent">Wczytaj</button>
    <button mat-raised-button (click)="save()"  color="accent">Zapisz</button>
    <button mat-raised-button (click)="clear()" color="accent">Wyczyść</button>`,
})
export class DataToolbarComponent {
  constructor(private transactionsService: TransactionsService,
    private _electronService: ElectronService, private dmService: DataModelService) { }

  import() {
    if (this._electronService.isElectron()) {
      const {dialog} = remote;
      const path = dialog.showOpenDialog({title: 'Otwórz historię rachunku', filters: [{name: 'Excel', extensions: ['xls']}]});
      this.transactionsService.readWorkbook(path);
    }
  }

  load() {
    this.dmService.load();
  }

  save() {
    this.dmService.save();
  }

  clear() {
    this.dmService.clear();
  }

}

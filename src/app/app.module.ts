// import 'zone.js/dist/zone-mix';
// import 'reflect-metadata';
// import 'polyfills';
import { BrowserModule } from '@angular/platform-browser';
import {ApplicationRef, NgModule} from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { DatePipe } from '@angular/common';

import { MatTableModule } from '@angular/material';
import {MatChipsModule} from '@angular/material';
import { MatInputModule } from '@angular/material';
import {MatButtonModule} from '@angular/material';
import {MatTabsModule} from '@angular/material';
import {MatDialogModule} from '@angular/material';
import {MatToolbarModule} from '@angular/material';
import {MatSortModule} from '@angular/material';
import {MatAutocompleteModule} from '@angular/material';
import {MatSelectModule} from '@angular/material';

import { NgxElectronModule } from 'ngx-electron';
import { ChartsModule } from 'ng2-charts';
import { NgPipesModule } from 'angular-pipes';

import { AppRoutingModule } from './app-routing.module';

import { ElectronService } from './providers/electron.service';
import { TransactionsService } from './providers/transactions.service';
import { DataModelService } from './providers/data-model.service';
import { TagsService } from './providers/tags.service';
import { RulesService } from './providers/rules.service';

import { AppComponent } from './app.component';
import { ChipsComponent } from './components/chips/chips.component';
import { TransactionsTableComponent } from './components/transactions-table/transactions-table.component';
import { TrasactionDetailsDialogComponent } from './components/trasaction-dialog/trasaction-details-dialog.component';
import { BalanceChartComponent } from './components/balance-chart/balance-chart.component';
import { HeaderComponent, DataToolbarComponent } from './components/header/header.component';
import { ReceiversTableComponent } from './components/receivers-table/receivers-table.component';
import { RulesComponent } from './components/rules/rules.component';
import { TagsTableComponent } from './components/tags-table/tags-table.component';
import { SpendingsChartComponent } from './components/spendings-chart/spendings-chart.component';
import { removeNgStyles, createNewHosts } from '@angularclass/hmr';


@NgModule({
  declarations: [
    AppComponent,
    ChipsComponent,
    TransactionsTableComponent,
    DataToolbarComponent,
    TrasactionDetailsDialogComponent,
    BalanceChartComponent,
    HeaderComponent,
    ReceiversTableComponent,
    RulesComponent,
    TagsTableComponent,
    SpendingsChartComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    HttpModule,
    AppRoutingModule,
    MatTableModule,
    NoopAnimationsModule,
    NgxElectronModule,
    ChartsModule,
    MatInputModule,
    MatChipsModule,
    MatButtonModule,
    MatTabsModule,
    MatDialogModule,
    MatToolbarModule,
    MatSortModule,
    MatAutocompleteModule,
    MatSelectModule,
    NgPipesModule
  ],
  providers: [ElectronService, TransactionsService, DatePipe, TagsService, DataModelService, RulesService],
  bootstrap: [AppComponent],
  entryComponents: [TrasactionDetailsDialogComponent]
})
export class AppModule {
  constructor(public appRef: ApplicationRef) {}
  hmrOnInit(store) {
    if (!store || !store.state) { return; }
    console.log('HMR store', JSON.stringify(store, null, 2));
    // restore input values
    if ('restoreInputValues' in store) { store.restoreInputValues(); }
    this.appRef.tick();
    Object.keys(store).forEach(prop => delete store[prop]);
  }
  hmrOnDestroy(store) {
    const cmpLocation = this.appRef.components.map(cmp => cmp.location.nativeElement);
    // recreate elements
    store.disposeOldHosts = createNewHosts(cmpLocation);
    // save input values
    // remove styles
    removeNgStyles();
  }
  hmrAfterDestroy(store) {
    // display new elements
    store.disposeOldHosts();
    delete store.disposeOldHosts;
  }
}

import { TransactionsTableComponent } from './components/transactions-table/transactions-table.component';
import { BalanceChartComponent } from './components/balance-chart/balance-chart.component';
import { ReceiversTableComponent } from './components/receivers-table/receivers-table.component';
import { RulesComponent } from './components/rules/rules.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { TagsTableComponent } from './components/tags-table/tags-table.component';
import { SpendingsChartComponent } from './components/spendings-chart/spendings-chart.component';

const routes: Routes = [
  { path: '', redirectTo: 'list', pathMatch: 'full' },
  { path: 'list/:query', component: TransactionsTableComponent },
  { path: 'list', component: TransactionsTableComponent },
  { path: 'balance-chart', component: BalanceChartComponent },
  { path: 'spendings-chart', component: SpendingsChartComponent },
  { path: 'receivers', component: ReceiversTableComponent },
  { path: 'rules', component: RulesComponent },
  { path: 'rules/:predicate', component: RulesComponent },
  { path: 'tags-table', component: TagsTableComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { useHash: true })],
  exports: [RouterModule]
})
export class AppRoutingModule { }

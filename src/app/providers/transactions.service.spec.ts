/// <reference types="jasmine" />
import {TransactionsService} from './transactions.service';
import {DataModelService, Transaction} from './data-model.service';
import {RulesService} from './rules.service';
import * as fs from 'fs';
import {ElectronService} from './electron.service';
import {Deferred} from 'ts-deferred';
import {async} from '@angular/core/testing';

describe('transactionsService', () => {
  let trService: TransactionsService;
  let dataModelServiceStub = { dataModel: { transactions: [] }};
  let rulesServiceStub = { apply(trs) {}};
  let electronServiceStub = { readFile(path) { return fs.readFileSync('test_resources/workbook.xls'); } }


  beforeEach(() => {
    trService = new TransactionsService(
      dataModelServiceStub as DataModelService,
      rulesServiceStub as RulesService,
      electronServiceStub as ElectronService);
  });

  it('dummy test', () =>
    expect(1).toBeGreaterThanOrEqual(0));

  it('should return transactions', () => {
    expect(trService.transactions).toBe(dataModelServiceStub.dataModel.transactions);
  });

  it('should read workbook', () => {
    // given
    const spy = spyOn(rulesServiceStub, 'apply');

    // when
    trService.readWorkbook('foo');

    // then
    expect(spy.calls.first().args[0].length).toBe(3);
  });



});

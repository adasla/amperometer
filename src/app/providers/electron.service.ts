import { Injectable } from '@angular/core';

// If you import a module but never use any of the imported values other than as TypeScript types,
// the resulting javascript file will look as if you never imported the module at all.
import { ipcRenderer } from 'electron';
import * as childProcess from 'child_process';
import * as fs from 'fs';

@Injectable()
export class ElectronService {

  constructor() {
  }

  public isElectron = () => {
    return true;
  }

  public readFile(path) {
    return fs.readFileSync(path);
  }

}
